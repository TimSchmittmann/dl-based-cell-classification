This repository is based on the [code](https://codeocean.com/capsule/9068249/tree/v1) from [Matek et al.](https://gitlab.com/TimSchmittmann/dl-based-cell-classification/-/blob/master/9.pdf).

The classification code was transformed to work inside an IPython notebook and some scripts were added to handle different image sizes and test the model on some of the [previously segmented images](https://gitlab.com/TimSchmittmann/dl-based-image-cell-segmentation-with-mask-rcnn).  

The [Notebook](https://gitlab.com/TimSchmittmann/dl-based-cell-classification/-/blob/master/predict_leucocyte_class.ipynb) should be able to run inside colab without any further actions. 

If you want to recreate the environment and run the notebook locally try to use 

    pip install -r requirements.txt
    
which was created from [pipreqs](https://pypi.org/project/pipreqs/). Otherwise [pipfreeze.requirements.txt](https://gitlab.com/TimSchmittmann/dl-based-cell-classification/-/blob/master/pipfreeze.requirements.txt) contains all the installed packages from the colab environment. 